<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class MainController extends Controller
{
    private function geTop(){
        $top=User::all()->sortByDesc('rate');
        if($top->count()>10)
            $top->take(10);
        return $top;
    }

    public function index(){
        $top=$this->geTop();
        return view('main', compact('top'));
    }
}


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/reset.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <title>Document</title>
    <style>
        .crossTable { border-spacing:0px;  border-collapse: collapse; }
        .cellEmpty {  padding: 0px; }
        .cellNumber { padding: 1px; background-color: #FFFFFF; border: 0px solid #000000; width: 20px; height: 20px; }
        .cellLetter { padding: 1px; background-color: #EEEEEE; border: 1px solid #000000; width: 20px; height: 20px; }
        .cellDebug { padding: 1px; border: 1px solid #000000; width: 20px; height: 20px; }
        .crossTableA { border-spacing:0px;  border-collapse: collapse; }
        .cellEmptyA {  padding: 0px; }
        .cellNumberA { padding: 1px; background-color: #FFFFFF; border: 0px solid #000000; width: 30px; height: 30px; }
        .cellLetterA { padding: 1px; background-color: #EEEEEE; border: 1px solid #000000; width: 30px; height: 30px; }
        .cellDebugA { padding: 1px; border: 1px solid #000000; width: 30px; height: 30px; }
        .crossTableB { border-spacing:0px;  border-collapse: collapse; }
        .cellEmptyB {  padding: 0px; }
        .cellNumberB { padding: 1px; background-color: #FFFFFF; border: 0px solid #000000; width: 10px; height: 10px; }
        .cellLetterB { padding: 1px; background-color: #EEEEEE; border: 1px solid #000000; width: 10px; height: 10px; }
        .cellDebugB { padding: 1px; border: 1px solid #000000; width: 10px; height: 10px; }
    </style>
</head>
<body>
<header>
    <div class="header_logo-section">
        <div class="logo-section_logo">
            <img src="" alt="Logotype" class="logo-img">
        </div>
        <div class="logo-section_heading">
            <h1 class="main-heading">CrossWorld</h1>
        </div>
    </div>
    <nav class="header_nav nav">
        <ul class="nav_list">
            <li class="nav_list_item"><a href="" class="header-link">FAQ</a></li>
            <li class="nav_list_item"><a href="" class="header-link">Admin Panel</a></li>
        </ul>
    </nav>
    <div class="header_auth auth">
        @guest
        <a href="{{ route('login') }}" class="auth_login-link header-link">{{ __('Login') }}</a>
            @if (Route::has('register'))
        <a href="{{ route('register') }}" class="auth_reg-link header-link">{{ __('Register') }}</a>
            @endif
        @else
            <a  class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @endguest
    </div>
</header>


        <main >
            @yield('content')
        </main>
<footer>
    <ul class="footer_list-links">
        <li class="footer_list-links_item"><a class="footer-link" href="">Home</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Not home</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Sample link</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Sample long link</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Sample very very very very long link</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Sixth</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Ha! New one</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Another one</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">9</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">10</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">11</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">12</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">13</a></li>
    </ul>
</footer>
</body>
</html>

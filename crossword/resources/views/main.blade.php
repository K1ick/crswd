


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/reset.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <title>Document</title>
    <style>
        .crossTable { border-spacing:0px;  border-collapse: collapse; }
        .cellEmpty {  padding: 0px; }
        .cellNumber { padding: 1px; background-color: #FFFFFF; border: 0px solid #000000; width: 20px; height: 20px; }
        .cellLetter { padding: 1px; background-color: #EEEEEE; border: 1px solid #000000; width: 20px; height: 20px; }
        .cellDebug { padding: 1px; border: 1px solid #000000; width: 20px; height: 20px; }
        .crossTableA { border-spacing:0px;  border-collapse: collapse; }
        .cellEmptyA {  padding: 0px; }
        .cellNumberA { padding: 1px; background-color: #FFFFFF; border: 0px solid #000000; width: 30px; height: 30px; }

        .cellDebugA { padding: 1px; border: 1px solid #000000; width: 30px; height: 30px; }
        .crossTableB { border-spacing:0px;  border-collapse: collapse; }
        .cellEmptyB {  padding: 0px; }
        .cellNumberB { padding: 1px; background-color: #FFFFFF; border: 0px solid #000000; width: 10px; height: 10px; }
        .cellLetterB { padding: 1px; background-color: #EEEEEE; border: 1px solid #000000; width: 10px; height: 10px; }
        .cellDebugB { padding: 1px; border: 1px solid #000000; width: 10px; height: 10px; }
    </style>
</head>
<body>
@php

    $col_zap=4999;

    function getRealIpAddr() {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            { $ip=$_SERVER['HTTP_CLIENT_IP']; }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            { $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; }
        else { $ip=$_SERVER['REMOTE_ADDR']; }
    return $ip;
    }

    if (strstr($_SERVER['HTTP_USER_AGENT'], 'YandexBot')) {
    $bot='YandexBot';
    }
    elseif (strstr($_SERVER['HTTP_USER_AGENT'], 'Googlebot')) {
    $bot='Googlebot';
    }
    else {
     $bot=$_SERVER['HTTP_USER_AGENT'];
      }

    $ip = getRealIpAddr();
    $date = date("H:i:s d.m.Y");
    $home = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $lines = file('C:\xampp\htdocs\crossword\resources\views\stats.log');
    while(count($lines) > $col_zap)
        array_shift($lines);
    $lines[] = $date."|".$bot."|".$ip."|".$home."|\r\n";
    file_put_contents('C:\xampp\htdocs\crossword\resources\views\stats.log', $lines);
@endphp
    <header>
        <div class="header_logo-section">
            <div class="logo-section_logo">
                <img src="" alt="Logotype" class="logo-img">
            </div>
            <div class="logo-section_heading">
                <h1 class="main-heading">CrossWorld</h1>
            </div>
        </div>
        <nav class="header_nav nav">
            <ul class="nav_list">
                <li class="nav_list_item"><a href="/info" class="header-link">FAQ</a></li>
            </ul>
        </nav>
        <div class="header_auth auth">
            @guest
                <a href="{{ route('login') }}" class="auth_login-link header-link">{{ __('Login') }}</a>
                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="auth_reg-link header-link">{{ __('Register') }}</a>
                @endif
            @else
                <a  class="auth_login-link header-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <a class="auth_reg-link header-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @endguest
        </div>
    </header>


    <main >

        <div class="main_content">@php

                set_time_limit(0);


                $pc=new App\Lib\PHP_Crossword(15,15);
                $pc->setGroupID('demo');
                $pc->setMaxWords(5);
                $success = $pc->generate();

                if(!$success){
                echo 'error';
                }
                else{
                $html = $pc->getHTML();
                $words = $pc->getWords();
                echo $html;
                }


        @endphp</div>
    <div class="main_sidebar sidebar">
        <div class="sidebar_user-rating user-rating">
            @guest
                <h3>Для просмотра своего рейтинга <a href="{{ route('login') }}">зайдите в аккаунт</a></h3>
            @else
            <h2 class="user-rating_heading">Мой рейтинг:{{Auth::user()->rate}}</h2>
            @endguest
        </div>
        <div class="sidebar_users-top users-top">
            <h2 class="users-top_heading">Топ 10</h2>
            <ul>
                @foreach($top as $ones)
                    <li>{{$ones->name}}:{{$ones->rate}}</li>
                @endforeach
            </ul>
        </div>
    </div>
</main>
<footer>
    <ul class="footer_list-links">
        <li class="footer_list-links_item"><a class="footer-link" href="">Home</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Not home</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Sample link</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Sample long link</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Sample very very very very long link</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Sixth</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Ha! New one</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">Another one</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">9</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">10</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">11</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">12</a></li>
        <li class="footer_list-links_item"><a class="footer-link" href="">13</a></li>
        <p>© Группапо пд такая-то, 2019</p>
    </ul>
</footer>
</body>
</html>

